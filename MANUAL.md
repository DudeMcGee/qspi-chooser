# Manual referring to v1.0 of the QSPI-Chooser (choosy)

Release 1.5, 2024-04-09

**Licenses:** 

The stuff written by myself (partially adopted from ZZswap by Nigel) is licensed under the GPL, see document COPYING. Parts of this project are the work of SwissMicros, their code is licensed under the **BSD 3-Clause License** which you find in the document LICENSE.

# Short Explanation

In order to save time and effort, as well as for me as for you, too, I hereby introduce the abbreviation FTI. It shall stand for Follow The Instructions (on the screen). It doesn't make sense to tell a user, press EXIT, press EXIT again, confirm with ENTER, press EXIT... This is so boring, and moreover, it is displayed on the screen and you have no alternative! So, if the system stops and shows you a screen with some more or less important information, then do what it tells you to, to continue with your plans! I'll only describe the really important steps from now on, when a sequence of robot actions follows, it is abbreviated to FTI, independently from how many keystrokes are necessary to reach the next level.

To use choosy, you only need the file **choosy.pgm** which resides in the **build folder** of the source-code tree! 

If you never used choosy before, create the directory qspi on the flash drive (activate USB disk). Download the pgm file and the corresponding QSPI file from the website which hosts the firmware you want to explore.

You have to prepare two files: the pgm (program) file you want to load instead of the running firmware plus the corresponding or associated QSPI file. The pgm file must be put in the root of the calculator's flash drive, the QSPI file must be copied into a directory on the flash drive, named qspi.

In case of the C47 you find everything in the zip archive which is linked to the homepage of the project, https://47calc.com.

## QSPI-Chooser, the installation

All you need to do, described in a short list:

1. Create the directory /qspi in the root directory of the DM42's flash drive.

2. For every firmware, be it DM42, C47, DB48X or other programs, get the pertinent pair of files, i.e. the QSPI file and the PGM file. The QSPI file for the DM42 software (or firmware) is in a subfolder of the firmware folder on the Swissmicros website.

3. Rename the files in a meaningful manner! E.g., C47_v0.109.pgm and C47_v0.109_qspi.bin.

4. Put each QSPI file in the /qspi folder.

5. Put each PGM file in the root folder.

6. Put choosy.pgm in the root folder.

Done!

# How To Swap Firmware (Dual-Boot)

In version 1.0 the process has been simplified so far that only two steps have to be performed in choosy: Pressing F5 to choose the QSPI file and pressing F6 to choose the PGM file. All the other keystrokes are either "press any key" or "press ENTER to confirm".

Once you have prepared the files, do this:

## DM42 to C47

01. Choose SETUP
02. 5. System
03. 2. Enter System Menu
04. 4. Reset to DMCP menu
05. 3. Load Program: Choose choosy (see next section!)! FTI
06. F5: Select QSPI file, e.g. C47_qspi.bin, copy process starts automatically, FTI
07. After a reboot choosy appears again, F6 to load PGM file C47...pgm!
08. 3. Load Program: Choose C47.pgm, e.g. FTI
09. C47 is running!
10. You may load one of your state files now!  

## C47 to DM42

01. Call DMCP
02. 3. Load Program
03. Select choosy.pgm (see next section!) and press ENTER, FTI
04. When choosy appears, press F5 and load the DM42 qspi file!
05. The system reboots automatically and writes the QSPI data into memory. FTI
06. When choosy re-appears, press F6 to select the PGM file! Choose the DM42...pgm file! FTI
07. DM42 starts with Memory Clear
08. Choose SETUP
09. 2. Calc. State
10. 1. Load State: Choose your state file
11. Done! The DM42 software is running again.

# Some Details

The QSPI area (part of the calculator's memory) is needed by most programs which can be run on the DM42. E.g., the DM42 software which is the software loaded onto the calculator when you receive it from Swissmicros, needs certain start-up data. If this is missing the software won't start.

So you may get the idea even now: qspi and pgm files come in pairs! When you load a QSPI file for the C47, every attempt to run the DM42 software will fail. 

It is important to name the files accordingly. Then you can easily choose the correct QSPI file and load the corresponding pgm file after the reboot.

Use a scheme like this:

```
C47v1.pgm
C47v1_qspi.bin
```

Since the calculator after a reboot will always run the most recently loaded program file, i.e. the one which ran before the reboot, choosy will re-appear once after the new QSPI file has successfully been copied into the root of the flash drive. At the time of this writing I doubt that this can be changed. But in this world of programming it is sometimes only one good idea away and you come to a solution no-one has ever heard of before. So, I'll keep on trying to find a solution! 

## The DMCP File Chooser

When you, e.g., use the DMCP command Load Program you're presented with a small file explorer window. In here there are two functions you possibly didn't hear about:

F5: This key switches between bytes and Kbytes.

F6: This key switches between two-column and one-column view which comes in very handy when you have to deal with longer filenames!

# Nigel's ZZSwap

Without Nigel, without his development of ZZSwap, this project had never come to life! I'm thankfully using big parts of this program even now, especially the copy function he had to develop, works wonderfully! And you do need some expertise to write something like this!

So, thanks again, Nigel!

And this is his project link:

https://gitlab.com/njdowrick/zzswap