- [VORWORT](#vorwort)
- [Unterstützung für deutsche Anwender](#unterstützung-für-deutsche-anwender)
- [QSPI-Chooser](#qspi-chooser)
- [QSPI-Chooser, die Installation](#qspi-chooser-die-installation)
  - [Das choosy.pgm-File](#das-choosypgm-file)
  - [Handbuch](#handbuch)
  - [Version 1.5](#version-15)
  - [Version 1.0](#version-10)
- [Technischer Abschnitt für Programmierer](#technischer-abschnitt-für-programmierer)


## VORWORT

Der QSPI-Chooser (interner pgm und Kosename choosy) is ein voll ausgebauter FIRMWARE-TAUSCHER für die Hardware-Plattform DM42 von Swissmicros.

Der Zweck des Programms ist der schnelle Wechsel von einer Firmware zur anderen, z. B. vom DM42 zum C47 und zurück, vom C47 zum DM42.

Das Programm unterstützt jegliche alternative Firmware, die per QSPI-Verfahren geladen wird.

Mit der aktuellen Version sind die Tastendrücke des Anwenders auf ein absolutes Minimum reduziert! Die wesentlichen Befehle werden jetzt nicht mehr über das DMCP-Menü ausgeführt, sondern direkt im QSPI-Chooser per Funktionsaufruf. Das beschleunigt den ganzen Prozeß, und die nötigen Tastendrücke werden sozusagen vom Programm überflüssig gemacht. 

## Unterstützung für deutsche Anwender

Dieses Projekt soll auch deutschen Anwendern ohne Sprachhürde zugänglich sein. Die Umstellung auf zwei Sprachen ist im Gang, ein deutschsprachiges Manual (lateinisch für Handbuch) ist bereits erstellt worden. Die README.md habt Ihr gerade vor Euch.

## QSPI-Chooser

Das Projekt basiert auf Nigels ZZSwap, einem ersten kleinen Programmierprojekt zum Thema Dual-Boot. Zwecks besserer Unterscheidbarkeit habe ich mich entschlossen, den Namen ZZSwap aufzugeben und nicht zu übernehmen, obwohl er natürlich genial erdacht war :) Danke, Nigel, für die Vorarbeit und den Spaß an der Sache!

In meinem eigenen Gitlab-Projekt kann ich natürlich auch nichts kaputtmachen, das war ebenfalls ein Gedanke dabei.

Nigels Ausgangsprojekt findet Ihr hier:

https://gitlab.com/njdowrick/zzswap

## QSPI-Chooser, die Installation 

Siehe MANUAL_de.md!

### Das choosy.pgm-File

Im **build**-Unterverzeichnis des Projektbaums liegt die Datei **choosy.pgm**. Die müßt Ihr herunterladen, mehr wird nicht benötigt, um das Programm zu benutzen. Weitere Infos in MANUAL_de.md.

### Handbuch

Es gibt zwei Handbücher, MANUAL.md (englisch) und MANUAL_de.md (deutsch).

### Version 1.5

Diese Version vermeidet ein Problem mit dem DM42, das bislang aufgetreten war: Nach dem Wechsel vom C47 zum DM42 meldete der DM42 ein **Memory Clear**. Wenn man dann ein Statusfile laden wollte, kam die Fehlermeldung, daß er nicht auf die sogenannte state area zugreifen könne. Man mußte dann einen clean state laden, erst danach konnte man seine eigenen Status-Files laden. 

Version 1.5 vermeidet das Problem. Der DM42 startet zwar immer noch mit **Memory Clear**, aber ohne die Fehlermeldung, wenn man dann versuchte, seinen gewünschten Status zu laden. Das funktioniert jetzt einwandfrei.

### Version 1.0

Version 1.0 führt die beiden entscheidenden DMCP-Funktionsaufrufe selbst aus und reduziert damit das nötige Maß an Anwendereingriffen auf ein Minimum.

## Technischer Abschnitt für Programmierer

Wer an den technischen Details interessiert ist, um selber mit dem Code zu experimentieren, sollte sich das englische README.md ansehen.

Der Code kann auch sehr gut skelettiert werden, um ein eigenes pgm zu schreiben, das ganz andere Aufgaben ausführen soll.

---=== EoF ===---