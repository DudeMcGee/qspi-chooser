# The Choosy Wiki

This document should give an overview over the state of the project and related things such as hints for using etc.

Short remark before we start: choosy can switch between an arbitrary number of pairs consisting of a pgm and a qspi file. The number is of course limited by the storage capacity on the FAT drive of the calculator. In my case, as an example, I have two C47 versions (one standard release and one beta) as well as a DM42 version stored, and can switch between the three. So, dual boot is not quite correct :)

## What's the deal here? Dual-Boot?

There are basically two ways to switch between two or more different firmware versions or calculator software variants like Free42 (= DM42), DB48X, C47 and others: You may use an external computer as auxilliary device by which you swap files on the calculator's flash drive, OR you install all the necessary files on the DM42 and let it run a program file (.pgm file) by its own which asks the user for a QSPI file and after a reboot for the pgm file to run.

The former method using the PC is widely known because you need it to update your calculator. This is normally done using a so-called combo-file, which combines the software (DM42) with the QSPI data. That's the preferred way Swissmicros offer their updates.

Using a pair consisting of a QSPI file AND a pgm file is not so popular but very flexible. Therefore it is often used by 3<sup>rd</sup> party software.

The typical user of a calculator doesn't need to know about this special stuff, but the ones who love experimenting will appreciate it: They can now switch between different calculators on the fly without the need of an extra computer!

I've just switched from the C47 to the alpha version, and it took me `1 minute and 15 seconds`! (With QSPI-Chooser)

## To be taken into account!

Today it's October, 30. and a few days ago I stumbled across this issue: When you attempt to load QSPI data to flash the QSPI data area and have no connection to an external power source, like your computer's USB port or a power-bank, you'll be presented with this announcement on the calculator screen:
```
Connect USB to avoid battery 
draining during flashing
```
As soon as you do so, the announcement vanishes and the process is continued as normal.

At the time of this writing the least annoying but also 100% safe solution is the use of a power-bank. I say safe, because the calculator might freeze if power becomes too low during flashing. 

This issue most probably doesn't affect many of you, but I work on the subject.

## Current version

1.5

## QSPI-Chooser v1.0

QSPI-Chooser (choosy) needs only two calls to perform the whole task: 

First run: the user presses F5 to choose the QSPI file. After copying the program reboots the calculator.

Second run: the user presses F6 to choose the PGM file. After loading the calculator reboots automatically again.

Ready! I don't think that further simplification is possible, also speed is at a maximum now. But you never know what future keeps in store, maybe I or another guy have an even better idea, we'll see! At the time being QSPI-Chooser is the by far most elegant and fastest method to swap firmware on a DM42!

## How to achieve all this?

### Backup and Status File (state file)

First of all, perform a backup. I recommend to do this everytime, you have written a complex program or changed other important configuration details. The flash drive has 6 MB of capacity, that's nearly nothing compared to a standard laptop's drive. Simply put a copy of the whole tree onto your computer. I use folder names containing date & time.

### The State File, esp.!

It is also recommended to write a new state file before you perform the backup. Why?

Because the DM42 is prone to start with `Memory Clear`! I've never experienced this when I switched back to the C47, but switching to the DM42 very often shows this incorrect start-up behaviour.

If you've backed up the calculator, installed an alternative firmware and now switch back to the DM42 using one of the secure and approved recipes to switch back to the DM42 software, it may start with a `Memory Clear`. The remedy is: load a so-called clean state, and after this, load one of your trusty old state files. Done! Problem solved.

### Installation of an alternative calculator software (firmware)

I'll explain two different methods, one comprehensive way and one lean fast-lane approach.

### Comprehensive installation of the C47

Prepare a complete backup of your DM42!

Download the zip archive from 47calc.com and extract it. Create a new directory and copy all of the files for the DM42, the complete directory tree, into this new folder.

Create a new directory and copy the complete backup of the DM42 into it. Then copy the C47 files/directory tree over it, in other words, merge the trees in a new one. You can do this since DM42 and C47 use different naming schemes. Both systems happily share the same tree.

To finalize, create a folder named /qspi. Put the QSPI bin files of DM42 and C47 into this folder. The QSPI file for the DM42 can be found in the firmware section of the DM42 of the Swissmicros website. 

QSPI files always have the extension .bin, and their name must contain the substring `"_qspi"`. Regular names are `xx_qspi_23.bin` or `abc_qspi.bin`. But `x3qspi_.bin` is illegal! 

Last measure: Copy the .pgm files of DM42 and C47 into the root dir of the flash drive.

Oops, nearly forgot to mention: The file choosy.pgm should also reside in the flash drive's root! You'll finde choosy.pgm in the build folder of the project QSPI-Chooser!

Now delete the flash drive contents completely and put your new tree onto it.

That's all for the comprehensive method.

### Minimal installation of the C47 

Create the /qspi folder, put the necessary files there, copy the DM42's .pgm together with the C47's .pgm into the root of the flash drive. Also copy choosy.pgm there. Done.

## Links to further information

English:<br>
https://c47-calculator.net/2024/03/23/dm42-dual-boot-a-new-project/ Dual-Boot, a new project<br>
https://c47-calculator.net/2024/03/20/zzswap-next-step-after-dual-boot/ zzswap<br>
https://c47-calculator.net/2024/03/17/the-quickest-and-most-convenient-way-to-switch-back-and-forth-between-a-dm42-and-a-c47/ Quick switch<br>
https://c47-calculator.net/2024/03/16/how-to-transform-a-dm42-into-a-c47-and-back-safely/ DM42 and C47, transformation<br><br>
https://c47.miraheze.org/wiki/Main_Page Another Wiki on the C47<br>
https://c47.miraheze.org/wiki/Exploring_The_C47_Calculator_On_DM42_Hardware C47, explore<br><br>

Deutsch (German):<br>
https://c47-calculator.weebly.com/dm42-zu-c47-kann-da-was-schiefgehen.html DM42 zu C47, Risiken?<br>
https://c47-calculator.weebly.com/dm42-zu-c47-und-zuruumlck-schnellstmoumlgliches-verfahren.html Schnellstmöglicher Wechsel C47, DM42<br>
https://c47-calculator.weebly.com/zzswap-mama-mama-ganz-ohne-computer.html zzswap, ganz ohne Computer<br>
https://c47-calculator.weebly.com/dmcp-system-setup-eine-kurze-erklaumlrung.html DMCP, Erläuterung<br>
https://c47-calculator.weebly.com/waumlhl-dein-programm-qspi-chooser.html QSPI-Chooser<br><br>

## Important note!

This is work in progress and not finalized yet! More information to follow, so stay tuned! 