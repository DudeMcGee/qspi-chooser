# Das Choosy-Wiki

Dieses Dokument soll einen Überblick über das Projekt geben und damit zusammenhängende Dinge schildern, z. B. Hinweise zur Benutzung u. ä.

Kurze Vorbemerkung: Choosy kann zwischen einer beliebigen Anzahl von Paaren, bestehend aus einem pgm- und einem qspi-File, wechseln. Die Zahl ist natürlich limitiert durch die Speicherkapazität des FAT-Laufwerks auf dem Taschenrechner. Um ein Beispiel zu nennen: Ich habe zur Zeit drei Programme an Bord, das reguläre C47-Release sowie eine Beta, dazu die DM42-Software. Dual-Boot trifft es also nicht ganz :)

## Worum geht es? Dual-Boot?

Grundsätzlich existieren zwei Methoden, um zwischen zwei Firmware-Varianten zu wechseln, z. B. zwischen der Originalsoftware DM42 (= Free42 von Thomas Okken) und dem C47: Man kann einen externen Computer zu Hilfe nehmen, mit dem man die nötigen Änderungen auf dem Flash-Drive des DM42-Taschenrechners vornimmt,  oder man führt ein Programm auf dem Taschenrechner selbst aus, bspw. QSPI-Chooser (choosy) oder ZZSwap.

Die Methode mit dem PC kennt jeder, weil man sie benutzt, um ein Update der Swissmicros-Software durchzuführen. Hierzu wird in aller Regel (aber durchaus nicht zwingend) ein sog. Combo-File benutzt, in dem die für den Startvorgang erforderlich QSPI-Daten zusammen mit dem Firmware-Programm zusammengefaßt sind. Dies ist das von Swissmicros vorgesehene Standardverfahren. Es hat auch einen entscheidenden Vorteil: Man braucht keinen Zwischenschritt, um nach dem Schreiben des QSPI-Sektors die eigentliche Taschenrechnersoftware zu laden, sondern der Rechner erledigt das in einem Durchgang.

Wenn man jedoch ein File mit dem QSPI-Daten hat und eines mit eigentlichen Programm, als pgm-Datei, dann werden zunächst die QSPI-Daten geschrieben, es erfolgt ein Reboot, und dann erst wird die Software (pgm) geladen. Das klingt jetzt erstmal nach einem unnötigen Schritt, der nur aufhält, in Wahrheit gewinnt man aber Flexibilität! Ich glaube, nahezu (?) alle Drittanbieter von Software für den DM42 verwenden diese Mehode.

Grundsätzlich benötigt man dazu also ein Paar von Dateien, jedes Paar besteht aus einer bin-Datei (QSPI) und einer pgm-Datei. Diese Paare gehören zwingend zusammen.

Für den normalen Anwender mag dies alles uninteressant sein, weil er ohnehin nur seine DM42-Software benutzt. Dem Interessierten erlaubt es aber, verschiedene Rechner auf seinem DM42 zu installieren und nach Herzenslust zu experimentieren! Das Umschalten zwischen zwei vorinstallierten Softwarevarianten dauert im Regelfall nur zwischen einer und zwei Minuten, höchstens! Ich selbst habe z. Z. drei Varianten auf dem Rechner, C47, C47-alpha und DM42. Normalerweise schalte ich in ca. einer Minute um.

Ich habe gerade umgeschaltet vom C47 zur Alpha-Version: Es kostete mich ohne Streß `1 Minute und 15 Sekunden`! (Mit QSPI-Chooser)

## Was es zu beachten gilt!

Heute ist der 30. Oktober 2024, und vor ein paar Tagen stolperte ich erstmals über ein Problem, das beim Laden der QSPI-Daten auftritt, wenn der Rechner nicht an eine externe Stromversorgung angeschlossen ist (USB-Port am PC, Power-Bank etc.):

Es erscheint eine Meldung, die lautet: 
```
Connect USB to avoid battery 
draining during flashing 
```
Auf Deutsch: Verbinden Sie den USB-Port, um zu vermeiden, daß während des Flashens der Saft ausgeht :)

Die Meldung verschwindet erst, wenn man das tut, und dann wird der Prozeß auch umgehend korrekt fortgesetzt.

Zur Zeit ist die Verwendung einer Power-Bank die empfohlene Vorgehensweise. Der Vorteil ist, daß es auch die sichere Methode ist, denn wenn die Energieversorgung während des Flashens zusammenbricht, wird der Rechner einfrieren.

Das Thema wird die meisten von Euch gar nicht berühren, aber ich arbeite daran.

## Aktuelle Version

1.5

## QSPI-Chooser v1.0

Der QSPI-Chooser (choosy) benötigt jetzt nur noch zwei Aufrufe: Beim ersten Aufruf drückt der Anwender F5, um das QSPI-File zu laden. choosy bootet automatisch! choosy erscheint nach dem Reboot erneut, nun drückt man F6, um das PGM-File zu laden. choosy bootet erneut, und der Rechner startet mit der neuen Software! Das ist eine erhebliche Vereinfachung gegenüber den Vorgängerversionen, der Prozeß geht jetzt derart schnell vonstatten, daß eine weitere Vereinfachung des Vorgangs meiner Meinung nach nicht mehr möglich ist. Aber man weiß ja nie, auf was für Ideen man selber noch kommt oder auch andere, die es noch besser machen wollen :) !

## Wie macht man das alles?

### Backup und Status-Datei

Als erstes muß man ein Backup anfertigen. Das würde ich ohnehin in regelmäßigen Abständen tun. Beispiel: Wenn ich ein neues Programm geschrieben habe, das mich viel Arbeit gekostet hat, mache ich schnell ein Backup. Der Flash-Drive des DM42 hat eine (vom User direkt nutzbare) Kapazität von 6 MB, das ist nicht viel. Ich differenziere also nicht groß, sondern kopiere einfach den kompletten Verzeichnisbaum vom DM42 in ein Verzeichnis, das im Namen Datum und Uhrzeit enthält.

### Status-Datei im Besonderen!

Es ist ebenfalls sinnvoll, vor dem Backup ein aktuelles Status-File anzulegen! Speziell die Software des DM42 neigt dazu, beim Start ein `Memory Clear` anzuzeigen. Und das, obwohl sie eigentlich in demselben sauberen Zustand hochfahren müßte wie vor dem Wechsel. Die Ursachen sind unbekannt, ich weiß nur, daß es mir beim C47 noch nie passiert ist, beim DM42 hingegen oft.

Nehmen wir also an, Ihr habt ein Backup gemacht, habt dann den C47 installiert, und dann habt Ihr eine der sicheren Methoden verwendet, den DM42 wiederherzustellen. Aber der zeigt Euch nach dem erfolgreichen Start den Default-Zustand und die Meldung `Memory Clear`. Dann ladet einen sogenannten sauberen Status, einen clean state. Danach ladet Ihr eines Eurer Status-Files, und voilá: Der alte, gewohnte DM42 im zuletzt benutzten Zustand.

### Installation einer alternativen Software

Am Beispiel einer C47-Installation gehen wir mal die aufwendigste Installationsmethode durch, dann die weniger aufwendige.

### Komplettinstallation C47

Legt ein vollständiges Backup vom DM42 an.

Ladet das zip-File von 47calc.com herunter und entpackt es. Legt ein eigenes neues Verzeichnis an, in das Ihr alle C47-Dateien, also den gesamten Baum, kopiert.

Erstellt ein neues Verzeichnis: In dieses kopiert Ihr das Backup vom DM42, und dann den Verzeichnisbaum des C47. So einen Vorgang nennt man merge, also praktisch ein Verweben zweier Verzeichnisbäume. Ihr könnt dies völlig problemlos tun, da C47 und DM42 unterschiedliche Dateibenennungen verwenden. Es wird also nichts überschrieben, sondern beide System teilen sich einfach nur platzsparend einen Verzeichnisbaum.

Das einzige, was jetzt erforderlich ist, um die Sache abzuschließen: Erstellt in diesem Verzeichnisbaum, der dann gleich auf den Flash-Drive kopiert wird, ein Verzeichnis /qspi. In dieses Verzeichnis müssen das QSPI-File für den C47 und das für den DM42. Letzteres findet Ihr bei Swissmicros im Firmware-Verzeichnis. 

QSPI-Dateien haben immer die Endung bin! UND, ganz wichtig, im Namen müssen sie den Substring `"_qspi"` enthalten. Zulässige Namen sind also bspw. `xx_qspi_23.bin` oder `abc_qspi.bin`. Aber nicht `x3qspi_.bin`! 

Letzte Maßnahme: Die .pgm-Datei des DM42 und die des C47 sollten im Wurzelverzeichnis des Flash-Drives liegen! Kopiert sie dorthin!

Oh, fast vergessen: Was auch dort liegen sollte, ist die Datei choosy.pgm des QSPI-Chooser-Projekts. Ihr findet sie im build-Verzeichnis. Einfach herunterkopieren.

Nun löscht alles auf dem Flash-Drive und kopiert diesen neu erstellten Verzeichnisbaum auf den DM42.
Das ist alles, das war die aufwendige Methode.

### Minimalinstallation C47 

Legt den /qspi-Ordner mit den nötigen Dateien an, kopiert die .pgm des DM42 ins Wurzelverzeichnis zusammen mit der .pgm des C47. Kopiert auch choosy.pgm dorthin, Fertig.

## Wie geht's weiter?

Am besten lest Ihr jetzt wenigstens das MANUAL hier im Projektverzeichnis (MANUAL.md)!

## Links zu weiterführenden Informationen

English:<br>
https://c47-calculator.net/2024/03/23/dm42-dual-boot-a-new-project/ Dual-Boot, a new project<br>
https://c47-calculator.net/2024/03/20/zzswap-next-step-after-dual-boot/ zzswap<br>
https://c47-calculator.net/2024/03/17/the-quickest-and-most-convenient-way-to-switch-back-and-forth-between-a-dm42-and-a-c47/ Quick switch<br>
https://c47-calculator.net/2024/03/16/how-to-transform-a-dm42-into-a-c47-and-back-safely/ DM42 and C47, transformation<br><br>
https://c47.miraheze.org/wiki/Main_Page Another Wiki on the C47<br>
https://c47.miraheze.org/wiki/Exploring_The_C47_Calculator_On_DM42_Hardware C47, explore<br><br>

Deutsch (German):<br>
https://c47-calculator.weebly.com/dm42-zu-c47-kann-da-was-schiefgehen.html DM42 zu C47, Risiken?<br>
https://c47-calculator.weebly.com/dm42-zu-c47-und-zuruumlck-schnellstmoumlgliches-verfahren.html Schnellstmöglicher Wechsel C47, DM42<br>
https://c47-calculator.weebly.com/zzswap-mama-mama-ganz-ohne-computer.html zzswap, ganz ohne Computer<br>
https://c47-calculator.weebly.com/dmcp-system-setup-eine-kurze-erklaumlrung.html DMCP, Erläuterung<br>
https://c47-calculator.weebly.com/waumlhl-dein-programm-qspi-chooser.html QSPI-Chooser<br><br>

## Wichtig!

Die Arbeit hier ist noch nicht abgeschlossen, schaut von Zeit zu Zeit vorbei! 