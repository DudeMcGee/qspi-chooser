## Version history

### 1.5

This release avoids this problem: When you switched from the C47 to the DM42, the latter nearly always started with **Memory Clear**. When you then tried to load a state file, it couldn't read the so-called state area. This is no longer the case: Immediately after the start with **Memory Clear** you can now call SETUP and load your preferred state file.

Unfortunately, I still could not find a method to load the most recent state file automatically. So, in case of the C47 and the DM42 the user has to load his preferred state file by himself. Maybe some day this can be changed... 

### 1.1

Includes a hint on the frontpage and in addition an extra info page regarding the DMCP file selector. This selector opens with 2 columns as per default. I have a feeling that it is not widely known that there are two functions available which change the file selector display:

F5: This key toggle switches between bytes and Kbytes.

F6: This key toggle switches between a 2 column narrow view and a 1 column wide view. If the user has chosen to give meaningful names to his files the 2 column view can be suboptimal. The wide view instead should be sufficient in most cases to show the complete file names.

### 1.0

This was the first version which comprised loading of the QSPI file as well as loading of the PGM file. The first version which felt feature-complete to me.

```
   +---------------------------------------------------+
   | End of version history                            |  
   | Earlier development steps are not documented here |
   +---------------------------------------------------+
```

## The Early Roots: ZZSwap by Nigel (UK)

Since this project wouldn't exist without Nigel who developed the first pgm, back then called **ZZswap** :) , here's a copy of his original README.md:

This program is intended to run on the SwissMicros DM42.

Several different calculator programs are available to run on the DM42
hardware: e.g., the original Free42-based calculator; WP34S; WP43; C47;
db48x. The problem with exiting one and running another is that each
program in this list (with the exception of the WP34S) needs its own QSPI file
to be loaded into memory before it runs. When a QSPI file is loaded
the DM42 deletes it from its disk, so it can't be used again without
reloading it from a computer.

This program manages QSPI files so that a user can change from one
program to another without needing to connect to a computer.

Before use, the zzswap.pgm file needs to be copied from the
build/ folder here onto the DM42 disk. The user should also create a
folder called `qspi` (lower case) at the top level of the disk, and copy all of the
QSPI files for the programs they are interested in into this
folder. The .pgm files for each of these programs should also be added
to the top level folder of the disk.

All of the QSPI files should have the file extension `.bin` (which
they do anyway, I think). They
should also have recognisable names so that it is clear which file
belongs with which program. The free space remaining on the calculator
disk after these files are added must be at least equal to the size of
the largest QSPI file, and preferably considerably more.

You are strongly advised to use this program only when connected to
external power. This doesn't have to be a computer; any usb power
source will do. If you don't do this and your battery is low, you risk
corrupting the calculator's disk. (I think the calculator forces you to
connect to power in any case before flashing QSPI.)

Make a backup of your calculator's disk before you start! You're doing
this doing this regularly in any case. Aren't you?

Suppose you're running the C47 calculator and you want to switch to
Free42 (the original 42S software). This is what you do.

* Exit from C47 using SYSTEM in the
MODE menu. This (I believe) saves the calculator's state
automatically, and takes you to the DMCP menu.

* Select Option 3 (Load Program). Press the up-arrow once and you'll be
on zzswap.pgm (that's the reason for its name!). Press ENTER, then
ENTER again in the usual way.

* The calculator erases its flash memory to make way for the new
program. This takes several seconds. After that, press any key, then
EXIT, just as the calculator instructs. zzswap should start!

* At this point, you can immediately leave it, ask it for help (very
basic), or continue. Let's assume you continue by pressing any key
other than F1, EXIT, or Backspace.

* You'll see a list of `.bin` files in the qspi folder and will be invited
to select one of them. You know that the Free42 program's QSPI file is
called `DM42_qspi_` followed by a version number, so move the highlight
to that file with the arrow keys and press ENTER.

       You'll get a message telling you that the file is being copied,
    together with an indication of progress. DMCP doesn't provide a copy
    command, so the file is copied by the program 64 kB at a time.

    When it's done, you'll get a message. Now you need to:
    
* Press any key to leave the program. This takes you back to the DMCP
  menu.

* Select Option 4 - Load QSPI from FAT. If you aren't connected to USB
  the calculator will ask you to; I'm not sure if you can get around
  this.

* Once the QSPI file has been loaded, it will disappear from the
  top-level folder and you'll be invited to restart the calculator. Do
  so.

* Press EXIT when the flash screen appears; press EXIT again, and
  zzswap reloads! We don't want this, so press EXIT one last time to
  quit it.

* We're back to the DMCP menu. Now, at last, we can chose Option 3 -
  Load Program - and this time we choose the program that we want to
  run - `DM42-some_version_number.pgm`. Go through the usual ENTER and
  EXIT pressing associated with loading a program, and you're there.

DM42 seems to load with a blank state for me, but it's easy to reload
a previous state that you saved before quitting it, and there you are.

