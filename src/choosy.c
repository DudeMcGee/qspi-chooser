#include "math.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include <main.h>
#include <dmcp.h>

// Functions defined in main.h:
int check_f (uint f, const char* s);
uint nd_copy (const char* qspi_name, const char* new_name);
int copy_selected_file (const char * fpath, const char * fname, void * data);
void help_screens (void);
void key_wait_and_clear (void);
char print_string[64];

#define FPT ppgm_fp //use this as the file pointer
#define NO_DISP_NEW 0
#define NO_OVERWRITE_CHECK 0

void program_main() { // This is our main event loop!
  int c, fss, sf;

  // LBL start: 
  start:  

  lcd_clear_buf();
  lcd_writeClr(t24);
  lcd_writeClr(t20);

  t20->inv = 1;
  lcd_puts(t20, "          -=[ choosy, a f/w chooser ]=-          -");
  t20->inv = 0;
  lcd_putsAt(t20, 1, "   Do you have a current backup?");
  lcd_putsAt(t20, 2, "   Do you have a current state file?");
  lcd_putsAt(t20, 3, "   (In the FILE SELECTOR window you can");
  lcd_putsAt(t20, 4, "    use the F6 key to switch between");   
  lcd_putsAt(t20, 5, "    split-screen and wide-view!)");   
  t24->fixed = 1;
  lcd_putsAt(t24, 5, "F5  : Select QSPI file!");
  lcd_putsAt(t24, 6, "F6  : Select PGM file!");
  lcd_putsAt(t24, 7, "F1  : Info screens");
  lcd_putsAt(t24, 8, "EXIT: Quit!");
  t24->fixed = 0;
  lcd_refresh();

  do {
    sys_sleep(); // wait for key press
    c = key_pop();
  }
  while (c == 0); // go back to sleep if it's a key release
  
  // Bail out = QUIT
  if ( c == KEY_EXIT ) return;

  // F1: Show info screen sequence, then go to start
  if ( c == KEY_F1 ) {
    help_screens();
    goto start;
  }

  /***********************************************************************************
   * The following EXPERIMENTAL code works fine, but unfortunately, it has no effect:
   * The calculator software (C47 as well as DM42) just overwrites a given status
   * when the pgm is launched. Probably I have a misconception of how this works, 
   * probably changing the state file (the status) of a software is only reasonable
   * while this software is used. It makes no sense if choosy performs state changes
   * which are meaningless for itself.
   * 
   * I leave this code in here as a template for possible future use...
   ***********************************************************************************/
 
  /*
  // EXPERIMENTAL !
  // F4: Load state file
  if ( c == KEY_F4 ) {
    sf = file_selection_screen ("Choose state file", "/state", "", set_reset_state_file, NO_DISP_NEW, NO_OVERWRITE_CHECK, NULL);
    // if (sf == MRET_EXIT) return; // EXIT has been pressed
    // return;
    goto start;
  }
  */

  // F5: Load QSPI -> go to LBL go
  if ( c == KEY_F5 ) {
    goto go;
  }

  // F6: Load program, bail out
  if ( c == KEY_F6 ) {
    set_reset_magic(CLEAN_RESET_MAGIC);
    // Also in this place it won't work:
    // sf = file_selection_screen ("Choose state file", "/state", "", set_reset_state_file, NO_DISP_NEW, NO_OVERWRITE_CHECK, NULL);
    run_menu_item_sys(MI_PGM_LOAD);
    return;
  }

 // LBL go:
 go:
  fss = file_selection_screen ("Choose qspi file", "/qspi", ".bin", copy_selected_file, NO_DISP_NEW, NO_OVERWRITE_CHECK, NULL);
  if (fss == MRET_EXIT) return; // EXIT has been pressed

  /* Immediately after the QSPI file has been chosen by the user, 
  the copy process starts. Again immediately after this process
  has finished, the QSPI file gets loaded without any further
  user intervention! The trick is done by using the following
  command: */

  // F5 functions (QSPI):
  set_reset_magic(NO_SPLASH_MAGIC);
  run_menu_item_sys(MI_LOAD_QSPI);
}
// End of program_main()
//********************************************************

// copy_selected_file
int copy_selected_file (const char * fpath, const char * fname, void * data) { // callback function
  lcd_clear_buf();
  lcd_writeClr(t24);
  lcd_puts(t24, "Copying");
  lcd_putsAt(t24, 1, fname);
  lcd_putsAt(t24, 2, "to root directory.");
  lcd_refresh();
  if (nd_copy (fpath, fname)) {
      lcd_putsAt(t24, 5, "Copy failed.");
      return 2;
  }
  return 1;  
}
// End of copy_selected_file

// check_f
int check_f (uint f, const char* s) {
  
  if (f != 0) {
    sprintf (print_string, "%8i %8s\n", f, s);
    msg_box (t24, print_string, 0);
    lcd_refresh();
    wait_for_key_press();
    return 1;
  }
  return 0;
}
// End of check_f

// nd_copy
uint nd_copy (const char* qspi_name, const char* new_name) {

  char* buffer;
  int buffer_size = 65536, fileSize;
  uint bytes_read = 0, bytes_written = 0;
  uint bytes_read_here = 0, bytes_written_here = 0;
  uint x = 0;
  FRESULT f;
  
  // get a buffer - return 1 if fails
  buffer = (char *) calloc (buffer_size, 1);
  if (!buffer) {
    // perror("calloc failed");
    return 1;
  }

  // get filesize, checking that the file to copy exists.
  fileSize = file_size (qspi_name);
  if (fileSize == -1) return 2;

  do {
    f = f_open (FPT, qspi_name, FA_READ); // open region file for reading
    check_f (f, "ropen");
    f = f_lseek (FPT, bytes_read); // go to right bit
    check_f (f, "rlseek");
    f = f_read (FPT, buffer, buffer_size, &x); // read in one buffer size
    check_f (f, "read");
    bytes_read_here = x;
    f = f_close (FPT); // close for writing
    check_f (f, "rclose");

    sys_disk_write_enable(1);
    f = f_open (FPT, new_name, FA_WRITE | FA_OPEN_ALWAYS); // create / open new file for writing;
    check_f (f, "wopen");
    f = f_lseek (FPT, bytes_read); // go to right bit
    check_f (f, "wlseek");
    f = f_write (FPT, buffer, bytes_read_here, &x);
    check_f (f, "write");
    bytes_written_here = x;
    f = f_close (FPT);
    check_f (f, "wclose");
    sys_disk_write_enable (0);
    
    bytes_read += bytes_read_here; // update bytes read
    bytes_written += bytes_written_here; // update bytes read
    sprintf (print_string, "%12i bytes copied", bytes_written);
    msg_box (t24, print_string, 0);
    lcd_refresh();
  }
  while (bytes_written < fileSize);

  free (buffer);
  return f;
}
// End of nd_copy

// help_screens (info screen sequence)
void help_screens (void) {
  
  lcd_clear_buf();
  lcd_writeClr(t24);

  t24->inv = 1;
  lcd_puts(t24, "choosy INFO 1/5");
  t24->inv = 0;
  //
  lcd_putsAt(t24, 2, "This program lets a user choose"); 
  lcd_putsAt(t24, 3, "a QSPI file stored in /qspi");
  lcd_putsAt(t24, 4, "to copy to the top level folder.");
  lcd_putsAt(t24, 5, "The program reboots automatically");
  lcd_putsAt(t24, 6, "after the copy process.");
  //
  lcd_putsAt(t24, 8, "(Press any key to continue!)");
  key_wait_and_clear();

  t24->inv = 1;
  lcd_puts(t24, "choosy INFO 2/5");
  t24->inv = 0;
  //
  lcd_putsAt(t24, 2, "After the reboot the user presses");
  lcd_putsAt(t24, 3, "F6 to load the PGM file.");
  lcd_putsAt(t24, 4, "The program reboots for a second");
  lcd_putsAt(t24, 5, "time, then the new firmware");
  lcd_putsAt(t24, 6, "starts automatically.");
  //
  lcd_putsAt(t24, 8, "(Press any key to continue!)");
  key_wait_and_clear();

  t24->inv = 1;
  lcd_puts(t24, "choosy INFO 3/5");
  t24->inv = 0;
  //
  lcd_putsAt(t24, 2, "For this to work the QSPI");
  lcd_putsAt(t24, 3, "files must be copied into the");
  lcd_putsAt(t24, 4, "/qspi folder and the .pgm files");
  lcd_putsAt(t24, 5, "into the top level folder");
  lcd_putsAt(t24, 6, "ready for use.");
  //
  lcd_putsAt(t24, 8, "(Press any key to continue!)");
  key_wait_and_clear();

  t24->inv = 1;
  lcd_puts(t24, "choosy INFO 4/5");
  t24->inv = 0;
  //
  lcd_putsAt(t24, 2, "The FILE SELECTOR which is");
  lcd_putsAt(t24, 3, "used to choose QSPI and PGM file");
  lcd_putsAt(t24, 4, "shows 2 columns by default.");
  lcd_putsAt(t24, 5, "With F6 you can toggle switch");
  lcd_putsAt(t24, 6, "between 1 and 2 columns!");
  //
  lcd_putsAt(t24, 8, "(Press any key to continue!)");
  key_wait_and_clear();

  t24->inv = 1;
  lcd_puts(t24, "choosy INFO 5/5");
  t24->inv = 0;
  //
  lcd_putsAt(t24, 2, "Note that free disk space at");
  lcd_putsAt(t24, 3, "least as big as the biggest");
  lcd_putsAt(t24, 4, "QSPI file is needed!");
  lcd_putsAt(t24, 5, "");
  char str[30];
  strcpy(str, "Prgm: ");
  strcat(str, PROGRAM_NAME);
  strcat(str, ", version ");
  strcat(str, PROGRAM_VERSION);
  lcd_putsAt(t24, 6, str);
  lcd_putsAt(t24, 7, "Author: Dude McGee (HMB)");
  lcd_putsAt(t24, 8, "(Press any key to LEAVE info!)");

  key_wait_and_clear();
}
// End of help_screens (info screen sequence)

// key_wait_and_clear
void key_wait_and_clear () {
  int c;
  
  lcd_refresh();
  do {
    sys_sleep(); // wait for key press
    c = key_pop();
  }
  while (c == 0); // go back to sleep if it's a key release

  lcd_clear_buf();
  lcd_writeClr(t24);
  lcd_writeClr(t20);
}
// End of key_wait_and_clear
