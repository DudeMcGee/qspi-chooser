- [PREFACE](#preface)
- [QSPI-Chooser](#qspi-chooser)
- [QSPI-Chooser, the installation](#qspi-chooser-the-installation)
  - [The choosy.pgm file!](#the-choosypgm-file)
  - [Manual](#manual)
  - [Version 1.5](#version-15)
  - [Version 1.0](#version-10)
- [Technical Section For Programmers/Coders](#technical-section-for-programmerscoders)
  - [Prerequisites](#prerequisites)
  - [Build](#build)
  - [DMCP\_SDK repository](#dmcp_sdk-repository)
  - [Customization](#customization)
    - [Program name](#program-name)
    - [Makefile](#makefile)


## PREFACE

For German readers: Es gibt ein deutsches LIES_MICH.md gleichen Inhalts!

The QSPI-Chooser (internal pgm and nickname choosy) is a full-fledged FIRMWARE SWAPPER for the Swissmicros DM42 hardware platform.

The rationale is to offer a method to quickly swap firmware, e.g. from the DM42 to the C47 and backwards, from C47 to DM42.

The method can be used for every alternative firmware which uses the QSPI boot method.

The current development stage has already reduced keystrokes to an absolute minimum. DMCP commands which formerly had to be executed by the user, are now launched from within the program via function calls. This accelerates the whole process and the formerly needed keystrokes are not necessary anymore: the program takes the burden off the user.

## QSPI-Chooser

This project is based on Nigel's ZZSwap, see below. To allow for easier differentiation between Nigel's ZZSwap and my QSPIchooser (internally called choosy) the project name on Gitlab was changed. Unfortunately, I must say, because I do love this name, zzswap :) !

I've set up my own Gitlab project to start learning and changing the
source code step by step as I'm getting more familiar with it.

The intention to proceed in a separate project here was only, not to
mess up Nigel's original repository which can be found here:

https://gitlab.com/njdowrick/zzswap

## QSPI-Chooser, the installation

See MANUAL.md!

### The choosy.pgm file!

The only file the user needs resides in the **build folder** and goes by the name **choosy.pgm**! More information in MANUAL.md.

### Manual

There are two new manuals: MANUAL.md (English) and MANUAL_de.md (German)!

### Version 1.5

This release avoids this problem: When you switched from the C47 to the DM42, the latter nearly always started with **Memory Clear**. When you then tried to load a state file, it couldn't read the so-called state area. This is no longer the case: Immediately after the start with **Memory Clear** you can now call SETUP and load your preferred state file.

Unfortunately, I still could not find a method to load the most recent state file automatically. So, in case of the C47 and the DM42 the user has to load his preferred state file by himself. Maybe some day this can be changed... 

### Version 1.0

Version 1.0 performs the two essential DMCP calls by itself. The user's efforts are reduced to a minimum.

## Technical Section For Programmers/Coders

### Prerequisites

GNU ARM toolchain can be downloaded from
  https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads.
We are currently using Version 7-2018-q2-update Linux 64-bit.

Make is usually available in some base development package, but you
can install it directly, e.g. for Debian-like systems
  sudo apt-get install make

Basic shell utilities (usually present in system):
  bash, dd, sed, tac, printf, find, etc.

Some usually available aux utilities (could require separate installation):
  crc32, sha1sum

### Build

Add ARM toolchain bin/ directory to PATH.
(e.g. ~/arm/gcc-arm-none-eabi-7-2018-q2-update/bin)

Run make to build the program.

Generated program 
  build/TESTPGM.pgm

Contents of QSPI
  build/TESTPGM_qspi.bin

### DMCP_SDK repository

The latest version of DMCP_SDK is available at
  https://github.com/swissmicros/DMCP_SDK

### Customization

#### Program name

To change the program name edit following two places:

```
Makefile:4:TARGET = TESTPGM
src/main.h:4:#define PROGRAM_NAME    "TESTPGM"
```

#### Makefile

In general it is sufficient to add your source files and libraries
to the Makefile under "# Custom section".

---=== EoF ===---