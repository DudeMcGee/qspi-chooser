# Manual referring to v1.0 of the QSPI-Chooser (choosy)

Release 1.2, 2024-04-06

**Lizenzen:**

Der von mir verfaßte Code, der zum Teil auf dem Projekt ZZswap von Nigel beruht, ist lizenziert unter der GPL, Details im Text COPYING. Des weiteren gibt es Code von SwissMicros, der unter der **BSD 3-Clause License** freigegeben worden ist.

# Kurze Erläuterung vorab

Um Zeit und Arbeitsaufwand zu minimieren, sowohl für Euch als auch für mich, führe ich hiermit die Abkürzung FTI, die soviel bedeutet wie "follow the instructions (on the screen)". Zu deutsch: Folge den Anweisungen (auf dem Bildschirm). Um noch mehr Arbeit zu sparen, bleibe ich in der deutschen Übersetzung bei FTI. Vielleicht setzt sich diese Abkürzung auch im deutschen Sprachraum so durch wie das beliebte Read The Fine Manual. Anstatt also entsetzlich öde Anweisungen zu schreiben wie: Drück EXIT, drück nochmal EXIT, bestätige jetzt mit ENTER etc. etc..., schreibe ich also einmal FTI. Ganz einfach. Erst in dem Moment, in dem Ihr an einen Punkt kommt, wo man überhaupt eine Entscheidung über das weitere Vorgehen fällen kann, werde ich eine klare Anweisung geben. Ich denke, das macht's für beide Seiten viel, viel einfacher.

Um choosy zu verwenden, müßt Ihr **choosy.pgm** einfach aus dem **build**-Verzeichnis des Quellcodes herunterladen.

Wenn Ihr choosy noch nie verwendet habt, legt auf dem Flash-Drive ein Verzeichnis /qspi an! Das pgm-File Eurer alternativen Software kommt in das Wurzelverzeichnis des Flash-Drives, das dazugehörige QSPI-File kommt in das /qspi Verzeichnis. (Das QSPI-File für die DM42-Software liegt in einem Unterverzeichnis des Firmware-Verzeichnisses, das pgm-File liegt im Verzeichnis firmware bei Swissmicros.)

Alle Dateien für den C47 finden sich im zip-Archiv auf https://47calc.com.

## QSPI-Chooser, die Installation 

Alles, was Ihr tun müßt folgt in einer kurzen Liste:

1. Legt auf dem Flash-Drive des DM42 ein Verzeichnis mit dem Namen /qspi an. Mit anderen Worten: Auf der höchsten Ebene, dem Wurzelverzeichnis, erstellt Ihr /qspi.

2. Ihr müßt Euch für jede Software, die Ihr verwenden wollt, das QSPI-File und das PGM-File besorgen. Das QSPI-File zur DM42-Software findet Ihr in einem eigenen Unterverzeichnis des Firmware-Verzeichnisses von Swissmicros.

3. Gebt jeder Datei einen vernünftigen, bedeutungsvollen Namen: Z. B. C47_v0.109.pgm and C47_v0.109_qspi.bin.

4. Kopiert jedes QSPI-File in den /qspi-Ordner.

5. Kopiert jedes PGM-File (.pgm) ins Wurzelverzeichnis.

6. Kopiert choosy.pgm in das Wurzelverzeichnis.

Fertig!

# Firmware austauschen (Dual-Boot, Swap)

Der Prozeß ist jetzt mit der Version 1.0 so verschlankt, daß zum Austausch der Software nur zwei Schritte in choosy erforderlich sind, nämlich einmal F5 für das QSPI-File und einmal F6 für das PGM-File zu drücken. Dazwischen muß man nur den Anweisungen folgen (FTI), die sich auf "press any key" und "press ENTER to confirm" beschränken.

Wenn Ihr die Dateien auf dem Flash-Drive so gespeichert habt wie oben beschrieben, folgt einer dieser beiden Anleitungen:

## DM42 to C47

01. Starte SETUP
02. 5. System
03. 2. System Menu
04. 4. Reset to DMCP menu
05. 3. Load Program: Wähle choosy (siehe nächsten Abschnitt!)! FTI
06. F5: Select QSPI file, Wähle Dein C47_qspi.bin (Beispielname), der Kopierprozeß startet autom., FTI
07. Nach dem Reboot erscheint choosy erneut, wähle nun F6 um das PGM-File C47...pgm zu laden!
08. 3. Load Program: Wähle C47.pgm (Beispielname), FTI
09. C47 läuft!
10. Lade ggf. ein State-File!  

## C47 to DM42

01. Starte DMCP
02. 3. Load Program
03. Wähle choosy.pgm (siehe nächsten Abschnitt!) und drück ENTER, FTI
04. Wenn choosy erscheint, drück F5 und lade das DM42 QSPI-File!
05. Das System bootet automatisch und schreibt die QSPI-Daten ins Memory. FTI
06. Wenn choosy wieder erscheint, drück F6, um das PGM zu wählen! Wähle das DM42...pgm File! FTI
07. DM42 startet mit der Meldung **Memory Clear**
08. Starte SETUP
09. 2. Calc. State
13. 1. Load State: Wähle eines Deiner State-Files

# Ein paar Details

Die QSPI-Sektion (Teil des Memorys des Rechners) wird von den meisten Programmen benötigt, die auf dem DM42 verwendet werden können. Die Standardsoftware auf dem DM42 ist ja die gleichnamige Software, und sie benötigt bestimmte Daten in der QSPI-Sektion, um starten zu können. Durch Auslesen des QSPI-Files und anschließendes Schreiben in die QSPI-Sektion wird das bewerkstelligt.

Ihr merkt also schon: QSPI- und PGM-Datei kommen immer als Pärchen! Und das sollten sie bleiben.

Ihr tut Euch natürlich leichter, wenn Ihr die Dateien mit sprechenden Namen ausstattet. Die Programmdatei muß nur die Endung .pgm haben, die QSPI-Datei hingegen muß zum einen die Endung .bin besitzen, aber im Namen den Teilstring `_qspi` aufweisen. Im Idealfall hätte man dann z. B. so etwas:

```
C47v1.pgm
C47v1_qspi.bin
```

Da der DM42 beim Start (Reboot) immer das zuletzt aktive Programm neu lädt, erscheint choosy erneut, nachdem die QSPI-Daten geschrieben worden sind. Daher müßt Ihr es dann beenden mit EXIT und im DMCP-Menü 3. Load Program aufrufen. Daß choosy nochmal erscheint, ist also ein Verhalten des DM42 und kein Programmierfehler auf unserer Seite. Trotzdem wird nach einer Lösung gesucht, um diesen überflüssigen Schritt zu vermeiden.

## Der DMCP-File-Explorer

Wenn Ihr z. B. den Befehl Load Program ausführt, erscheint ein eigenes Fenster mit einem kleinen File-Explorer. Dieser hat interessante Funktionen, die vielleicht nicht jeder kennt:

F5: Diese Taste schaltet zwischen Bytes und KBytes um!

F6: Diese Taste schaltet zwischen zweispaltiger und einspaltiger Anzeige um, was sehr hilfreich sein kann, wenn man Dateien mit etwas längeren Namen differenzieren muß!

# Nigels ZZSwap

Ohne Nigels Arbeit an der Entwicklung von ZZSwap wäre dieses Projekt hier niemals gestartet worden! Ich verwende dankbar seine Grundlagen, speziell die von ihm erdachte Copy-Funktion, die wunderbar funktioniert. Und man muß schon ein bißchen Know-How haben, um so etwas auszutüfteln.

Daher noch einmal ganz herzlichen Dank an Dich, Nigel!

Und das ist der Link auf Nigels Projekt:

https://gitlab.com/njdowrick/zzswap